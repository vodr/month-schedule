import React from 'react'
import classNames from 'classnames';

const MonthScheduleMonthRow =  React.createClass({

  days: {
    1: 'Pondělí',
    2: 'Úterý',
    3: 'Středa',
    4: 'Čtvrtek',
    5: 'Pátek'
  },

  getFormattedDate: function(){
    return this.props.date + '.' + (this.props.month + 1);
  },

  getDayName: function() {
    return this.days[this.props.day];
  },

  render () {
    var cls = classNames({friday: this.props.day === 5});
    return <tr className={cls}>
            <td>{this.getFormattedDate()}</td>
            <td>{this.getDayName()}</td>
            <td></td>
          </tr>;
  }
});

export default MonthScheduleMonthRow;
