import React from 'react'

const MonthScheduleForm =  React.createClass({

  render () {
    return <div className="month-form">
            <button onClick={this.props.onPreviousClicked}><i className="fa fa-arrow-left" aria-hidden="true"></i></button>
            Rozpis
            <button onClick={this.props.onNextClicked}><i className="fa fa-arrow-right" aria-hidden="true"></i></button>
          </div>;
  }
});

export default MonthScheduleForm;
