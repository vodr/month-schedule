import React from 'react'
import MonthScheduleForm from './MonthScheduleForm'
import MonthScheduleMonth from './MonthScheduleMonth'

const MonthSchedule =  React.createClass({

  getInitialState: function() {
    var date = new Date();
    date.setMonth(date.getMonth()+2);
    return {month: date.getMonth(), year: date.getFullYear()};
  },

  incrementMonth: function(month, year){
    if(month === 11){
      month = 0;
      year++;
    }else{
      month++;
    }
    return {
      month: month,
      year: year
    };
  },

  decrementMonth: function(month, year){
    if(month === 0){
      month = 11;
      year--;
    }else{
      month--;
    }
    return {
      month: month,
      year: year
    };
  },

  handlePreviousClicked: function(){
    this.setState(this.decrementMonth(this.state.month, this.state.year));
  },

  handleNextClicked: function(){
    this.setState(this.incrementMonth(this.state.month, this.state.year));
  },

  render () {
    var nextMonth = this.incrementMonth(this.state.month, this.state.year);
    return <div>
        <div className="no-print">
          <MonthScheduleForm
          onPreviousClicked={this.handlePreviousClicked}
          onNextClicked={this.handleNextClicked}
          />
        </div>
        <div className="month-column">
          <MonthScheduleMonth
          month={this.state.month}
          year={this.state.year} />
        </div>
        <div className="month-column">
          <MonthScheduleMonth
          month={nextMonth.month}
          year={nextMonth.year} />
        </div>
        <div className="clearfix" />
    </div>;
  }
});

export default MonthSchedule;
