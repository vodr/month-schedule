import React from 'react'
import MonthScheduleMonthRow from './MonthScheduleMonthRow'

const MonthScheduleMonth =  React.createClass({

  getNumberOfDaysInMonth: function(month, year){
    return new Date(year, month + 1, 0).getDate();
  },

  getDay: function(month,year){
    return new Date(this.props.year, month, 1).getDay();
  },


  getTableRows: function(){
    var month = this.props.month;
    var year = this.props.year;
    var numberOfDays = this.getNumberOfDaysInMonth(month, year);
    var day = this.getDay(month, year);
    var rows = [];
    for(var i = 1; i <= numberOfDays; i++){
        if(day !== 0 && day !== 6){
          rows.push(<MonthScheduleMonthRow key={i} day={day} date={i} month={month} year={year} />)
        }
        day = this.incrementDay(day);
    }
    return rows;
  },

  incrementDay: function(day){
    if(day === 6){
      return 0;
    }
    return day+1;
  },

  render () {
    return <div>
    <div className="month-title">{this.months[this.props.month]} {this.props.year}</div>
      <table className="month-table">
        <thead>
          <tr><th className="day">Datum</th><th className="day">Den</th><th className="name">Jméno</th></tr>
        </thead>
        <tbody>
          {this.getTableRows()}
        </tbody>
      </table>
      </div>;
  },

  months: {
    0: 'Leden',
    1: 'Únor',
    2: 'Březen',
    3: 'Duben',
    4: 'Květen',
    5: 'Červen',
    6: 'Červenec',
    7: 'Srpen',
    8: 'Září',
    9: 'Říjen',
    10: 'Listopad',
    11: 'Prosinec',
   }
});

export default MonthScheduleMonth;
