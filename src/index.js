import React from 'react'
import { render } from 'react-dom'
import App from './modules/App'


render((
  <App></App>
), document.getElementById('app'))
